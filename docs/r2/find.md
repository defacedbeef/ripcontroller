## radare2 

### searching in memory

Assume following layout of stack:


```
[0x004008fc]> pxr @rsp
0x7ffc609b86e8  0x00007f5b06f1eb97   ....[... @rsp (/lib/x86_64-linux-gnu/libc-2.27.so) library R X 'mov edi, eax' 'libc-2.27.so'
0x7ffc609b86f0  0x0000000000000001   ........ rdi
0x7ffc609b86f8  0x00007ffc609b87c8   ...`.... rsi stack R W X 'out 0x91, al' '[stack]'
0x7ffc609b8700  0x0000000100008000   ........
0x7ffc609b8708  0x0000000000400910   ..@..... (LOAD0) (/home/deadbeef/projects/ripcontroller/pwns/pwnable.kr/echo2/echo2) rip sym.main program R X 'push rbp' 'echo2'
0x7ffc609b8710  0x0000000000000000   ........ r15
0x7ffc609b8718  0x462382b2c55c850d   ..\...#F @hit17_0
0x7ffc609b8720  0x00000000004006b0   ..@..... (LOAD0) (/home/deadbeef/projects/ripcontroller/pwns/pwnable.kr/echo2/echo2) r12 entry0 program R X 'xor ebp, ebp' 'echo2'
0x7ffc609b8728  0x00007ffc609b87c0   ...`.... r13 stack R W X 'add dword [rax], eax' '[stack]'
0x7ffc609b8730  0x0000000000000000   ........ r15
0x7ffc609b8738  0x0000000000000000   ........ r15
0x7ffc609b8740  0xb9db4305dd5c850d   ..\..C.. @hit17_1
0x7ffc609b8748  0xb8958fd10622850d   .."..... @hit17_2
0x7ffc609b8750  0x00007ffc00000000   ........
0x7ffc609b8758  0x0000000000000000   ........ r15
0x7ffc609b8760  0x0000000000000000   ........ r15
0x7ffc609b8768  0x00007f5b072fe733   3./.[... (/lib/x86_64-linux-gnu/ld-2.27.so) library R X 'add r14, 8' 'ld-2.27.so'
0x7ffc609b8770  0x00007f5b072e4638   8F..[... (/lib/x86_64-linux-gnu/libc-2.27.so) library R X 'adc byte [rsi + 9], ch' 'libc-2.27.so'
0x7ffc609b8778  0x0000000000086c12   .l......
0x7ffc609b8780  0x0000000000000000   ........ r15
0x7ffc609b8788  0x0000000000000000   ........ r15
0x7ffc609b8790  0x0000000000000000   ........ r15
0x7ffc609b8798  0x00000000004006b0   ..@..... (LOAD0) (/home/deadbeef/projects/ripcontroller/pwns/pwnable.kr/echo2/echo2) r12 entry0 program R X 'xor ebp, ebp' 'echo2'
0x7ffc609b87a0  0x00007ffc609b87c0   ...`.... r13 stack R W X 'add dword [rax], eax' '[stack]'
0x7ffc609b87a8  0x00000000004006d9   ..@..... (LOAD0) (/home/deadbeef/projects/ripcontroller/pwns/pwnable.kr/echo2/echo2) program R X 'hlt' 'echo2'
0x7ffc609b87b0  0x00007ffc609b87b8   ...`.... stack R W X 'sbb al, 0' '[stack]'
0x7ffc609b87b8  0x000000000000001c   ........
0x7ffc609b87c0  0x0000000000000001   ........ @r13 rdi
0x7ffc609b87c8  0x00007ffc609b91e6   ...`.... @rsi stack R W X 'invalid' '[stack]' (/home/deadbeef/projects/ripcontroller/pwns/pwnable.kr/echo2/echo2)
0x7ffc609b87d0  0x0000000000000000   ........ r15
0x7ffc609b87d8  0x00007ffc609b9228   (..`.... @rdx stack R W X 'push rbp' '[stack]' (CLUTTER_IM_MODULE=xim)
0x7ffc609b87e0  0x00007ffc609b923e   >..`.... stack R W X 'push rbx' '[stack]' (LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:
```

`pxr` - **p**rint, he**x**, with **r**eferences

Quest1 - find address of `0x00007f5b072fe733` value.  
First of all, you want to define the search range. We are pretty much interested in `stack`
```
[0x004008fc]> e search.in=?
raw
block
io.map
io.maps
io.sections
io.sections.write
io.sections.exec
io.sections.readonly
dbg.stack
dbg.heap
dbg.map
dbg.maps
dbg.maps.exec
dbg.maps.write
dbg.maps.readonly
anal.fcn
anal.bb
[0x004008fc]> 
```

Thus,

```
[0x004008fc]> e search.in=dbg.stack
```

And then:
```
[0x004008fc]> /v8 0x00007f5b072fe733
Searching 8 bytes in [0x7ffc60999000-0x7ffc609ba000]
hits: 1
0x7ffc609b8768 hit19_0 33e72f075b7f0000
[0x004008fc]> 
```


