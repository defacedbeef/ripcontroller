# Load time ELF VMA fragmentation

```
$ cat lib.c
int myglobal = 3;

void foo() {
    myglobal = 0xff;
}
$
```

```
$ objdump -M intel -D ./lib.so | grep foo -A 7
0000000000001100 <foo>:
    1100:       55                      push   rbp
    1101:       48 89 e5                mov    rbp,rsp
    1104:       48 8b 05 cd 2e 00 00    mov    rax,QWORD PTR [rip+0x2ecd]        # 3fd8 <myglobal@@Base-0x4c>
    110b:       c7 00 ff 00 00 00       mov    DWORD PTR [rax],0xff
    1111:       5d                      pop    rbp
    1112:       c3                      ret    
```


```
$ readelf -r 64/lib.so

Relocation section '.rela.dyn' at offset 0x3d8 contains 8 entries:
  Offset          Info           Type           Sym. Value    Sym. Name + Addend
000000003e38  000000000008 R_X86_64_RELATIVE                    10f0
000000003e40  000000000008 R_X86_64_RELATIVE                    10b0
000000004018  000000000008 R_X86_64_RELATIVE                    4018
000000003fd8  000500000006 R_X86_64_GLOB_DAT 0000000000004020 myglobal + 0
000000003fe0  000100000006 R_X86_64_GLOB_DAT 0000000000000000 _ITM_deregisterTMClone + 0
000000003fe8  000200000006 R_X86_64_GLOB_DAT 0000000000000000 __gmon_start__ + 0
000000003ff0  000300000006 R_X86_64_GLOB_DAT 0000000000000000 _ITM_registerTMCloneTa + 0
000000003ff8  000400000006 R_X86_64_GLOB_DAT 0000000000000000 __cxa_finalize@GLIBC_2.2.5 + 0
$
```

rela.dyn => load time relocation. So it means, go to base addr of just mapped ELF plus 0x3fd8
and put value base_address + `0x4020` in the place.

Now, in `radare2`, go to that offset and verify its emptiness.

```
[0x00003fd8]> s 0x3fd8
[0x00003fd8]> pd 1
            ;-- section..got:
            ;-- reloc.myglobal:
            ;-- .got:
            0x00003fd8      .qword 0x0000000000000000                  ; RELOC 64 myglobal @ 0x00004020 + 0x0 ; [18] -rw- section size 40 named .got
[0x00003fd8]>
```

Verify value `0x3` at offset `0x4020`

```
[0x00003fd8]> px 8@0x4020
- offset -   0 1  2 3  4 5  6 7  8 9  A B  C D  E F  0123456789ABCDEF
0x00004020  0300 0000 0000 0000                      ........
[0x00003fd8]>
```

So what happens at load time is as follows:

1. map segments into memory at random (ASLR) base addr
2. 'jump' to base addr + 0x3fd8 and put base value "addr + 0x4020" there
3. put this .got section as read-only (its default nowadays)
4. Voila.

Thus, loader has all ingredients to make exploitation harder. Assume offset to .got (0x3fd8) may remain same.
However the other one (0x4020), may be easily incremented by random value. RW section (.data) may be put at
another address, say "loader trampiline", not necessarily coalesced to other sections of mapped ELF file.
This way, Virtual Address Space is more fragmented but ELF exploitation become more difficult.
