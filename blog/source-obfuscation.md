# Source Obfuscation

It is friday evening... After five days of work I reminded one thing: source obfuscation.  
The fact is that I've not even spent much time to investigate this topic due to couple of reasons, but the very first one is:

1. "hiding" security assets in the obfuscated, but still plain forest, is a pretty much - bad idea.

Your true assets should be hidden at rest and in transit, secured by cryptography. 
Ideally, there should be no other side-channel to unwrap your assets than usage of corresponding cryptographic private key. Period.
To do so, please use cryptography with strong security level which ensures both integrity and confidentiality.  
If you are unfamiliar with two latter words, please do catchup. Please use also library that was already tested before. Do not run your own crypto.


## Stunnix

If you'll put "string obfuscation C" into duckduckgo, you will find stunnix. Let's test it out.


Here is a sample code:
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* pw = "TAJNE_HASLO";
int main(int ac, char** av) {
	if (memcmp(av[1],pw,strlen(pw)) == 0)
		return 1;
	return 0;
}
```

Which in turn, ends like this:

```bash
$ perl cxx-obfus -x xpg4 test.cpp 
switching filename/dirname hasher to 'prefix' due to trial limitations. Get non-trial edition to fix this.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
const char*ReplacementFor_pw="\x54\x41\x4a\x4e\x45\x5f\x48\x41\x53\x4c\x4f";int 
main(int ReplacementFor_ac,char**ReplacementFor_av){if(memcmp(ReplacementFor_av[
(0x1e5a+2144-0x26b9)],ReplacementFor_pw,strlen(ReplacementFor_pw))==
(0x709+5874-0x1dfb))return(0x96f+1350-0xeb4);return(0xef+1141-0x564);}
```

Come on.

```bash
$ echo -e "\x54\x41\x4a\x4e\x45\x5f\x48\x41\x53\x4c\x4f"
TAJNE_HASLO
$ 
```

Even though, offsets are here to mislead source code reader!  
This stuff ends however in the compiler/linker and this type of obfuscation ends at that phase.


Let's verify above mentioned statement with the binary world.  

```
[0x000005a0]> pdf @sym.main
/ (fcn) main 80
|   int main (int argc, char **argv, char **envp);
|           ; var char **s1 @ rbp-0x10
|           ; var int var_4h @ rbp-0x4
|           ; arg int argc @ rdi
|           ; arg char **argv @ rsi
|           ; DATA XREF from entry0 (0x5bd)
|           0x000006aa      55             push rbp
|           0x000006ab      4889e5         mov rbp, rsp
|           0x000006ae      4883ec10       sub rsp, 0x10
|           0x000006b2      897dfc         mov dword [var_4h], edi     ; argc
|           0x000006b5      488975f0       mov qword [s1], rsi         ; argv
|           0x000006b9      488b05500920.  mov rax, qword obj.ReplacementFor_pw ; [0x201010:8]=0x784 str.TAJNE_HASLO
|           0x000006c0      4889c7         mov rdi, rax                ; const char *s
|           0x000006c3      e8a8feffff     call sym.imp.strlen         ; size_t strlen(const char *s)
|           0x000006c8      4889c2         mov rdx, rax                ; size_t n
|           0x000006cb      488b0d3e0920.  mov rcx, qword obj.ReplacementFor_pw ; [0x201010:8]=0x784 str.TAJNE_HASLO
|           0x000006d2      488b45f0       mov rax, qword [s1]
|           0x000006d6      4883c008       add rax, 8
|           0x000006da      488b00         mov rax, qword [rax]
|           0x000006dd      4889ce         mov rsi, rcx                ; const void *s2
|           0x000006e0      4889c7         mov rdi, rax                ; const void *s1
|           0x000006e3      e898feffff     call sym.imp.memcmp         ; int memcmp(const void *s1, const void *s2, size_t n)
|           0x000006e8      85c0           test eax, eax
|       ,=< 0x000006ea      7507           jne 0x6f3
|       |   0x000006ec      b801000000     mov eax, 1
|      ,==< 0x000006f1      eb05           jmp 0x6f8
|      ||   ; CODE XREF from main (0x6ea)
|      |`-> 0x000006f3      b800000000     mov eax, 0
|      |    ; CODE XREF from main (0x6f1)
|      `--> 0x000006f8      c9             leave
\           0x000006f9      c3             ret
[0x000005a0]> 

```

The conclusion is simple and straighforward. Do **not** make a penny on the `stunnix` license, even though it comes up with local Web UI service (wtf!).  
