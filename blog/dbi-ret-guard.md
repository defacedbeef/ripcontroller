# DBI DEP Technique
___


DBI stands for **D**ynamic **B**inary **I**nstrumentation, DEP is **D**ata **E**xecution **P**revency.  
You may incorporate Intel's PIN technology to instrument binaries which are considered untrusted, e.g. comes from 3rd parties.  


### pintool

```c++

#include "pin.H"
#include <iostream>
#include <fstream>


KNOB<std::string> KnobFuncName(KNOB_MODE_APPEND, "pintool",
        "f", "func", "specify function name to be protected");

ofstream s_traceFile;

class RetGuard {


    uint64_t                m_retAddress;
    std::string             m_functionName;

    uint64_t getRetFromCtx(CONTEXT* ctx)
    {
        uint64_t returnAddress = *((uint64_t*)PIN_GetContextReg(ctx, REG_RSP));
        s_traceFile << "    ret: 0x" << hex << returnAddress << dec << endl;
        s_traceFile << "    rbp: 0x" << hex << PIN_GetContextReg(ctx, REG_RBP) << dec << endl;
        return returnAddress;
    }

    RetGuard() = delete;

public:
    explicit RetGuard(const std::string& functionName)
    {
        m_functionName = functionName;

        if (!s_traceFile.is_open())
            s_traceFile.open("ret_guard.log");
    }

    bool verifyContextIntegrity(CONTEXT* ctx)
    {
        s_traceFile << "[+] verifying " << m_functionName << " context integrity" << endl;
        if (getRetFromCtx(ctx) != m_retAddress)
        {
            s_traceFile << "[+] context integrity lost. Faulty function: " << m_functionName << endl;
            return false;
        }
        return true;
    }

    void saveContext(CONTEXT* ctx)
    {
        s_traceFile << "[+] saving ctx of " << m_functionName << endl;
        m_retAddress = getRetFromCtx(ctx);
    }

    std::string& functionName()
    {
        return m_functionName;
    }
};


VOID BeforeRtn(CONTEXT* ctx, RetGuard* rg)
{
    rg->saveContext(ctx);
}


VOID AfterRtn(CONTEXT* ctx, RetGuard* rg)
{
    if(!rg->verifyContextIntegrity(ctx))
    {
        abort();
    }
}


VOID Image(IMG img, VOID* v)
{
    RTN rtn;
    for (unsigned int i = 0; i < KnobFuncName.NumberOfValues(); i++)
    {
        std::string functionName = KnobFuncName.Value(i);
        rtn = RTN_FindByName(img, functionName.c_str());

        if(RTN_Valid(rtn))
        {
            RetGuard* rg = new RetGuard(functionName); //one time leak is no leak ;)
            RTN_Open(rtn);
            RTN_InsertCall(rtn, IPOINT_BEFORE, AFUNPTR(BeforeRtn), IARG_CONTEXT, IARG_PTR, rg, IARG_END);
            RTN_InsertCall(rtn, IPOINT_AFTER, AFUNPTR(AfterRtn), IARG_CONTEXT, IARG_PTR, rg, IARG_END);
            RTN_Close(rtn);
        }
    }
}


int main(int argc, char *argv[])
{

    if( PIN_Init(argc,argv) )
    {
        return -1;
    }

    PIN_InitSymbols();
    IMG_AddInstrumentFunction(Image, 0);
    PIN_StartProgram();

    return 0;
}
```

### Instrumented binary 


```c

/* gcc -o main main.c -fno-stack-protector */

#include <stdio.h>
#include <string.h>

void vuln2(char* av1) {
	char buffer[11];
	strcpy(buffer,av1);
}

void vuln1(char* av1) {
	char buffer[22];
	strcpy(buffer,av1);
}

int main(int ac, char** av) {
	vuln1(av[1]);
	vuln2(av[1]);
}
```


### pin invocation


You can instrument both functions with the `ret_guard` pintool in a following way:

```bash
$ ../../../pin -t obj-intel64/ret_guard.so -f vuln1 -f vuln2 -- ./main `perl -e 'print "A"x22'`
Aborted (core dumped)
```

The output file:

```
$ cat ret_guard.log 
[+] saving ctx of vuln1
    ret: 0x5649fa5e76b0
    rbp: 0x7ffc40d8cf90
[+] verifying vuln1 context integrity
    ret: 0x5649fa5e76b0
    rbp: 0x7ffc40d8cf90
[+] saving ctx of vuln2
    ret: 0x5649fa5e76c3
    rbp: 0x7ffc40d8cf90
[+] verifying vuln2 context integrity
    ret: 0x564900414141
    rbp: 0x4141414141414141
[+] context integrity lost. Faulty function: vuln2
$ 
```


