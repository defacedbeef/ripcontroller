#include <assert.h>
#include "klee/klee.h"

int compare(unsigned available, size_t amount) {
    if (available < (unsigned)amount)
    {
        return 0;
    }
    return 1;
}


int main() {
    unsigned available;
    size_t amount;

    klee_make_symbolic(&available, sizeof(available), "available");
    klee_make_symbolic(&amount, sizeof(amount), "amount");

    klee_assume(amount > available);
    klee_assert(compare(available, amount) == 0);
}


