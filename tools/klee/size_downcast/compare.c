#include <assert.h>
#include "klee/klee.h"

int compare(size_t available, size_t amount) {
    if (available < amount)
    {
        return 0;
    }
    return 1;
}


int main() {
    size_t  available;
    size_t amount;
    int ret;

    klee_make_symbolic(&available, sizeof(available), "available");
    klee_make_symbolic(&amount, sizeof(amount), "amount");

    klee_assume(amount > available);
    klee_assert(compare(available, amount) == 0);
}


