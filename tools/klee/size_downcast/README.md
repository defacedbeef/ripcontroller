# Formally proven bug


Here we want to formally prove, over symbolic execution a correctness of 
`compare()` function.

## issue

Examine following buggy implementation of compare functionality.

```C 
int compare(unsigned available, size_t amount) {
    if (available < (unsigned)amount) {
        return 0;     
    }     
    return 1; 
} 
```

Function takes two arguments of different types. `size_t` is then downcasted to
`unsigned` to make conditional statement. Of course, such coding practice
introduces various bugs, including security issues. Goal is to use KLEE to 
prove formally this bug.


## proof

```C 
int main() {

    unsigned available;
    size_t amount;

    klee_make_symbolic(&available, sizeof(available), "available");
    klee_make_symbolic(&amount, sizeof(amount), "amount");

    klee_assume(amount > available);     
    klee_assert(compare(available, amount)== 0); 
}
```

If `amount` value is always bigger than `available`, the `compare()` function
shall always return "0". To verify it, let's add a new constraint to the solver
(i.e. `klee_assume`)  in which `amount` value  is always larger than value held
in `available` variable. We may add an assertion `klee_assert` to above 
mentioned constraint. On the other hand, such constraint shall cover only a 
half of the codebase of `compare()` function. You can use an assertion or 
analyze number of covered paths.


## sample output

```
$ make
clang -o compare.bc compare.c -emit-llvm -c -g -O0 -Xclang -disable-O0-optnone  
klee compare.bc
KLEE: output directory is "size_downcast/klee-out-0"
KLEE: Using Z3 solver backend

KLEE: done: total instructions = 40
KLEE: done: completed paths = 1
KLEE: done: generated tests = 1
clang -o compare_bad.bc compare_bad.c -emit-llvm -c -g -O0 -Xclang -disable-O0-optnone  
klee compare_bad.bc
KLEE: output directory is "size_downcast/klee-out-1"
KLEE: Using Z3 solver backend
KLEE: ERROR: compare_bad.c:21: ASSERTION FAIL: compare(available, amount) == 0
KLEE: NOTE: now ignoring this error at this location

KLEE: done: total instructions = 47
KLEE: done: completed paths = 2
KLEE: done: generated tests = 2
$ 
```

We use two targets in a Makefile, compare and compare_bad. First contains the bug
while second do not.

Examine results on your own.

