# crypto1

### What do we know here?

- login:     'guest'
- pass:      '8b465d23cb778d3636bf6c4c5e30d031675fd95cec7afea497d36146783fd3a1'
- ciphertext:'f5396cbe5606ee8fc18bffd1cf6fdd04951eaa82fd805b41381863243b74611' \
             '60fab3f5ed0067680af3357bdbed352d7cbf9456ead7ec54b52bda9fe6d7aa3' \
             '5db8bee8945fd376489f815cf3cf07d8302ed2b4a461ce84f1cc46ae0e39a63' \
             'c8df361ba06eafdc5f9b36fe38c95db1572482b5a5db8fae8d394de084b1b7e' \
             '92a8'

- padding: '\x00' - this is odd.
- pw = sha256(id+cookie)
- cipher: AES_CBC 128

Sniffed len(AES_CBC_E(id-pw-cookie)) == 128 bytes. = 8 * 16 bytes blocks
'guest'              == 5 bytes
'-'                  == 1 byte
sha256().hexdigest() == 64 bytes
'-'                  == 1 byte

sum of known == 71 bytes.

sum of known + unknown => [112-128]  
71 + unknown => [112 - 128]  
unknown =>  [41 - 56]  
 
cookie len: 40->56

By extending 'guest' you can find exact size of cookie. In the case of the server, the size is '49'.

### plain vs cipher

We have ciphertext oracle. For each plaintext, service responds with ciphertext.


### What we do not know?

- IV
- Key
- Cookie


### AES CBC

From the wikipedia: *Ehrsam, Meyer, Smith and Tuchman invented the Cipher Block Chaining (CBC) mode of operation in 1976.[11] In CBC mode, each block of plaintext is XORed with the previous ciphertext block before being encrypted. This way, each ciphertext block depends on all plaintext blocks processed up to that point. To make each message unique, an initialization vector must be used in the first block.*


  

