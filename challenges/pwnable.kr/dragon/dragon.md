## dragon


### ghidra 

Once used, one can fall in love with `ghidra`. I must admit that the tool is impressive.
After few of tens minutes, I was able to create two new types describing structures used in this task, 
apply them into the pseudocode and after few additional clicks, decompiler yield following code snippet:

```
undefined4 PriestAttack(Hero *hero,Monster *monster)

{
  int iVar1;
  
  while( true ) {
                    /* Prints MamaDragon or BabyDragon */
    (*(code *)monster->PrintMonsterInfoAddr)(monster);
    (*(code *)hero->PrintPlayerInfoAddr)(hero);
    iVar1 = GetChoice();
    if (iVar1 == 2) {
      puts("Clarity! Your Mana Has Been Refreshed");
      hero->MagicPoints = 0x32;
      printf("But The Dragon Deals %d Damage To You!\n",monster->DamagePoint);
      hero->HealthPoints = hero->HealthPoints - monster->DamagePoint;
      printf("And The Dragon Heals %d HP!\n",(int)(char)monster->HealthPointsDiff);
      monster->HealthPoints = monster->HealthPointsDiff + monster->HealthPoints;
    }
    else {
      if (iVar1 == 3) {
        if (hero->MagicPoints < 0x19) {
          puts("Not Enough MP!");
        }
        else {
          puts("HolyShield! You Are Temporarily Invincible...");
          printf("But The Dragon Heals %d HP!\n",(int)(char)monster->HealthPointsDiff);
          monster->HealthPoints = monster->HealthPointsDiff + monster->HealthPoints;
          hero->MagicPoints = hero->MagicPoints + -0x19;
        }
      }
      else {
        if (iVar1 == 1) {
          if (hero->MagicPoints < 10) {
            puts("Not Enough MP!");
          }
          else {
            printf("Holy Bolt Deals %d Damage To The Dragon!\n",0x14);
            monster->HealthPoints = monster->HealthPoints + -0x14;
            hero->MagicPoints = hero->MagicPoints + -10;
            printf("But The Dragon Deals %d Damage To You!\n",monster->DamagePoint);
            hero->HealthPoints = hero->HealthPoints - monster->DamagePoint;
            printf("And The Dragon Heals %d HP!\n",(int)(char)monster->HealthPointsDiff);
            monster->HealthPoints = monster->HealthPointsDiff + monster->HealthPoints;
          }
        }
      }
    }
    if (hero->HealthPoints < 1) break;
    if (monster->HealthPoints < 1) {
      free(monster);
      return 1;
    }
  }
```

Which is nice, isn't it? We all must agree - the process of new type definition is better than in IDA.

Not to mention `afvn` in radare2...


## lazy mode - brute it first

You can become a lazy one due to various of reasons. To discover winning path you can actually brute it...

Once bruted, one can sum two plus two and discover all vulnerabilities posed into this binary.

```
from pwn import *
import random

call_system = p32(0x08048dbf)

p = process('dragon')

main_menu = 0
fighting = 1
game_won = 2


def game_state_detect(msg):
    for the_line in msg.splitlines():
        if the_line == 'Choose Your Hero':
            return main_menu
        if the_line == "Well Done Hero! You Killed The Dragon!":
            return game_won
    return fighting


while True:
    game_state = game_state_detect(p.recv())

    if game_state is main_menu:
        random_val = random.randint(1, 2)
    elif game_state is fighting:
        random_val = random.randint(1, 3)
    else:
        print "you won the game!"
        exit(0)
    p.sendline(str(random_val))
```
  
  

## The winning move

```
wining_move = [2, 2, 1, 3, 3, 2, 3, 3, 2, 3, 3, 2, 3, 3, 2]
```
