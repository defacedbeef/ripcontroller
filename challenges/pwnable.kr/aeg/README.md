## Automatic Exploit Generation

It was a great task and even greater pleasure to solve it.  
Following author's request, I won't share the solution, however.  
The solution is available only to authenticated users, sorry.
-----BEGIN PGP MESSAGE-----

hF4DOVH4nUz1rE0SAQdAkFEinsZupIIOyCqCrabR0Suk67XUNIbOz0pYXpQFfWQw
zh4Cop0lj2iY+8SaA0NOFsck7hwcyKnuQrgtftoFxCTeI8ML9RiCm600hsjn78T1
0mcBq73Y0w+HYG2+1icsu4OYzQH9FTKNOdAQoX94BwgXPqhexEmjEwX5mEiCPj2W
GXlS6JHkS1z0BCoKPjvK+QXSK64F3I9fqDuYUP884V6MWmEDRDuFlqnlx3sTT10I
k/biLxspfGCy
=B1uX
-----END PGP MESSAGE-----
