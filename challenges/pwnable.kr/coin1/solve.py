"""
Recursive binary search
"""

from pwn import *


def bin_search(n, proc):
    h = [i for i in range(n[0], n[1])]
    h1 = [i for i in range(n[0], (n[1] - n[0]) / 2 + n[0])]
    h2 = list(set(h) - set(h1))
    h2.sort()

    if len(h1) > len(h2):
        proc.sendline(" ".join([str(i) for i in h1]))
    else:
        proc.sendline(" ".join([str(i) for i in h2]))

    w1 = proc.recvline(keepends=False)
    if 'Correct!' in w1:
        return
    if 'Wrong' in w1:
        print w1
        exit(0)

    if int(w1) % 10 == 0:
        scope = (h1[0], h1[-1] + 1)
    else:
        scope = (h2[0], h2[-1] + 1)

    bin_search(scope, proc)


s = ssh(host='pwnable.kr', user='uaf', password='guest', port=2222)
p = s.remote('localhost', 9007)

p.recvuntil('3 sec... -')

for r in range(100):
    p.recvuntil('N=')
    N = int(p.recvuntil(' '))
    p.recvuntil('C=')
    C = int(p.recvline(keepends=False))
    bin_search((0, N), p)

print p.recv()


