# OTP

This time, small hints rather than complete solution.

### man 7 signal

```
Signal mask and pending signals
A signal may be blocked, which means that it will not be delivered until it is later unblocked.  Between the time when it is generated and when it is delivered a signal is said to be pending.

Each thread in a process has an independent signal mask, which indicates the set of signals that the thread is currently blocking.  A thread can manipulate its signal  mask  using  pthread_sig‐
mask(3).  In a traditional single-threaded application, sigprocmask(2) can be used to manipulate the signal mask.

A child created via fork(2) inherits a copy of its parent's signal mask; the signal mask is preserved across execve(2).

A signal may be generated (and thus pending) for a process as a whole (e.g., when sent using kill(2)) or for a specific thread (e.g., certain signals, such as SIGSEGV and SIGFPE, generated as a
consequence of executing a specific machine-language instruction are thread directed, as are signals targeted at a specific thread using pthread_kill(3)).   A  process-directed  signal  may  be
delivered  to  any  one of the threads that does not currently have the signal blocked.  If more than one of the threads has the signal unblocked, then the kernel chooses an arbitrary thread to
which to deliver the signal.

A thread can obtain the set of signals that it currently has pending using sigpending(2).  This set will consist of the union of the set of pending process-directed signals and the set of  sig‐
nals pending for the calling thread.

A child created via fork(2) initially has an empty pending signal set; the pending signal set is preserved across an execve(2).
```


### ulimit --help

```
ulimit: ulimit [-SHabcdefiklmnpqrstuvxPT] [limit]
    Modify shell resource limits.
    
    Provides control over the resources available to the shell and processes
    it creates, on systems that allow such control.
    
    Options:
      -S	use the `soft' resource limit
      -H	use the `hard' resource limit
      -a	all current limits are reported
      -b	the socket buffer size
      -c	the maximum size of core files created
      -d	the maximum size of a process's data segment
      -e	the maximum scheduling priority (`nice')
      -f	the maximum size of files written by the shell and its children
      -i	the maximum number of pending signals
      -k	the maximum number of kqueues allocated for this process
      -l	the maximum size a process may lock into memory
      -m	the maximum resident set size
      -n	the maximum number of open file descriptors
      -p	the pipe buffer size
      -q	the maximum number of bytes in POSIX message queues
      -r	the maximum real-time scheduling priority
      -s	the maximum stack size
      -t	the maximum amount of cpu time in seconds
      -u	the maximum number of user processes
      -v	the size of virtual memory
      -x	the maximum number of file locks
      -P	the maximum number of pseudoterminals
      -T	the maximum number of threads
```
