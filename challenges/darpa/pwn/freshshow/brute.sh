STACK_TOP=fff4c000

cntr=0

for (( ; ; ))
do
    CURRENT_TOP=$(./test | grep stack | awk '{print $1}' | awk -F "-" '{print $1}')
    let cntr=cntr+1
    if [ "$CURRENT_TOP" = "$STACK_TOP" ]; then
	    echo "Done in $cntr"
	    exit 1
    fi
done
