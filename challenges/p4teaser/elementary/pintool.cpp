#include <fstream>
#include <iomanip>
#include <iostream>
#include <string.h>
#include <stdint.h>
#include <map>

#include "pin.H"

static const char* outFileName = "flag.txt";
static ofstream outFile;
static char flag[104] = {0};

static const uint8_t ASCII_LOW = 0x20;
static const uint8_t ASCII_HIGH = 0x7E;
static uint8_t BRUTE_ASCII = ASCII_LOW;
static unsigned int accessOrderOffset = 0;
static ADDRINT FunctionCheckFlagAddr = 0;
static ADDRINT FunctionCheckFlagEnds = 0;

std::map<uint8_t, int> functionCoverage;

static const uint8_t accessOrder[] = { //dumped from ghidra
        64, 38, 67, 88,  21, 68, 95, 36, 39, 77, 101, 90, 6,  48, 23, 62, 81, 83, 79, 44, 25, 17, 52, 3,  29, 102, 28, 93,
        16, 43, 78, 9,   60, 70, 71, 65, 40, 12, 56,  13, 57, 37, 69, 7,  84, 58, 89, 91, 14, 82, 45, 76, 66, 98,  75, 47,
        97, 34, 80, 103, 86, 5,  74, 1,  18, 51, 72,  26, 8,  31, 42, 85, 49, 33, 54, 63, 46, 4,  41, 24, 73, 35,  30, 92,
        11, 87, 50, 53,  32, 99, 96, 19, 10, 15, 55, 100, 2,  59, 94, 61, 22, 0,  27, 20 };


UINT64 functionCounter = 0;

static bool isByteBruteCompleted() {
    return (BRUTE_ASCII == ASCII_HIGH + 1);
}

VOID DoCountFunctionCalls() {
    functionCounter++;
}

static int callApplicationFunction(THREADID tid, CONTEXT *ctxt) {
    int ret = 0;

    CALL_APPLICATION_FUNCTION_PARAM param;
    memset(&param, 0, sizeof(param));
    param.native = false;

    PIN_CallApplicationFunction(ctxt, tid, CALLINGSTD_DEFAULT,
                                AFUNPTR(FunctionCheckFlagAddr), &param,
                                PIN_PARG(int), &ret,
                                PIN_PARG(char * ), flag,
                                PIN_PARG_END());
    return ret;

}

static ADDRINT FindAddressOfRtn(IMG img, const string& rtnName) {
    RTN r = RTN_FindByName(img, rtnName.c_str());
    ASSERT(RTN_Valid(r), "Failed to find RTN " + rtnName);
    return RTN_Address(r);
}

/**
 * Instrument only direct branches or calls within checkFlag()
 */
VOID Instruction(INS ins, VOID *v) {

    ADDRINT addr = INS_Address(ins);
    if (addr < FunctionCheckFlagAddr || addr > FunctionCheckFlagEnds) {
        return;
    }

    if (INS_IsCall(ins)
        && INS_IsDirectBranchOrCall(ins)) {
        INS_InsertCall(ins, IPOINT_BEFORE, AFUNPTR(DoCountFunctionCalls), IARG_END);
    }
}



/**
 * checkFlag function instrumentation
 *
 * If rax == 1 => we've got correct flag -> terminate process.
 * if else, instrument rax so it will pass coverage parameter to the caller.
 */
VOID AfterCheckFlag(CONTEXT* ctxt) {

    ADDRINT ctr = (ADDRINT)functionCounter;
    ADDRINT rax = PIN_GetContextReg(ctxt, REG_RAX);

    if(rax == 1) {
        outFile << "Really good job: " << flag << endl;
        exit(0);
    }

    PIN_SetContextReg(ctxt, REG_RAX, ctr);
    functionCounter = 0;
}

/**
 *  main() function instrumentation.
 *  This is brute-force heart of this exploit.
 */
VOID AfterMain(THREADID tid, CONTEXT *ctxt) {

    int ret = 0;
    std::map<uint8_t, int>::iterator iter;

    while(1) { //loop will terminate by CheckFlag analysis function

        flag[accessOrder[accessOrderOffset]] = BRUTE_ASCII;
        ret = callApplicationFunction(tid, ctxt);
        functionCoverage[BRUTE_ASCII] = ret;

        if(isByteBruteCompleted()) {
            uint8_t discoveredByte = 0;
            int coverage= 0;

            for(iter = functionCoverage.begin(); iter != functionCoverage.end(); ++iter ) {
                if(iter->second > coverage) {
                    coverage = iter->second;
                    discoveredByte = iter->first;
                }
            }

            flag[accessOrder[accessOrderOffset]] = discoveredByte;
            functionCoverage.clear();
            accessOrderOffset++;
            BRUTE_ASCII = ASCII_LOW;
        }
        else {
            BRUTE_ASCII++;
        }
    }
}


VOID Image(IMG img, VOID* v) {
    if (!IMG_IsMainExecutable(img))
        return;

    FunctionCheckFlagAddr = FindAddressOfRtn(img, "checkFlag");
    RTN rtn = RTN_FindByAddress(FunctionCheckFlagAddr);
    ASSERTX(RTN_Valid(rtn));

    RTN_Open(rtn);
    FunctionCheckFlagEnds = FunctionCheckFlagAddr + RTN_Size(rtn);

    for (INS i = RTN_InsHead(rtn); INS_Valid(i); i = INS_Next(i))
    {
        Instruction(i, (VOID*)"AOTI");
    }


    for (INS i = RTN_InsHead(rtn); INS_Valid(i); i = INS_Next(i))
    {
        if (INS_IsRet(i))
        {
            REGSET regsIn;
            REGSET_AddAll(regsIn);
            REGSET regsOut;
            REGSET_Clear(regsOut);
            REGSET_Insert (regsOut, REG_RAX);
            RTN_InsertCall(rtn, IPOINT_AFTER, (AFUNPTR)AfterCheckFlag, IARG_PARTIAL_CONTEXT, &regsIn, &regsOut, IARG_END);
        }
    }
    RTN_Close(rtn);

    RTN mainRtn = RTN_FindByName(img, "main");
    ASSERTX(RTN_Valid(mainRtn));

    RTN_Open(mainRtn);
    RTN_InsertCall(mainRtn, IPOINT_AFTER, AFUNPTR(AfterMain), IARG_THREAD_ID, IARG_CONTEXT, IARG_END);
    RTN_Close(mainRtn);
}

VOID Fini(INT32 code, VOID *v) {
    outFile << functionCounter << endl;
}

int main(int argc, char * argv[]) {

    if (PIN_Init(argc, argv)) return -1;
    outFile.open(outFileName);

    PIN_InitSymbols();
    IMG_AddInstrumentFunction(Image, 0);
    PIN_AddFiniFunction(Fini, 0);
    PIN_StartProgram();
    
    return 0;
}
